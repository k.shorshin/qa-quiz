package com.natera.qa_quiz;

import com.natera.qa_quiz.configuration.properties.QaQuizProperties;
import com.natera.qa_quiz.util.TriangleFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthenticationTests {
    @Autowired
    private QaQuizProperties qaQuizProperties;

    @Autowired
    private TriangleFactory triangleFactory;

    private WebTestClient webTestClient;

    @BeforeAll
    public void cleanUp() {
        triangleFactory.deleteAllTriangles();
    }

    @BeforeEach
    public void setup() {
        webTestClient = WebTestClient
                .bindToServer()
                .baseUrl(qaQuizProperties.getUri())
                .build();
    }

    @Test
    public void GetTriangleById_noToken_isUnauthorised() {
        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build("all"))
                .exchange()
                .expectStatus().isUnauthorized();

    }

    @Test
    public void CreateTriangleUsingPOST_noToken_isUnauthorised() {
        webTestClient
                .post()
                .uri(qaQuizProperties.getTriangleApi())
                .exchange()
                .expectStatus().isUnauthorized();
    }
}

package com.natera.qa_quiz;

import com.natera.qa_quiz.configuration.properties.QaQuizProperties;
import com.natera.qa_quiz.domain.CreateTriangle;
import com.natera.qa_quiz.domain.Triangle;
import com.natera.qa_quiz.util.TriangleFactory;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestMethod;
import org.springframework.test.web.reactive.server.FluxExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TrianglePostGetDeleteTests {

    @Autowired
    private QaQuizProperties qaQuizProperties;

    @Autowired
    private TriangleFactory triangleFactory;

    private WebTestClient webTestClient;

    private final int trianglesMaxAmount = 10;

    @BeforeAll
    public void cleanUp() {
        triangleFactory.deleteAllTriangles();
    }

    @BeforeEach
    public void setup() {
        webTestClient = WebTestClient
                .bindToServer()
                .baseUrl(qaQuizProperties.getUri())
                .defaultHeader(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                .build();
    }

    @Test
    public void getAllTriangles_isOk() {

        triangleFactory.deleteAllTriangles();

        Triangle triangle1 = triangleFactory.createRandomTriangle();
        Triangle triangle2 = triangleFactory.createRandomTriangle();

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build("all"))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Triangle.class)
                .consumeWith(response -> {
                    List<Triangle> result = response.getResponseBody();
                    assertThat(result.size(), equalTo(2));

                });

        triangleFactory.deleteTriangle(triangle1);
        triangleFactory.deleteTriangle(triangle2);

    }

    @Test
    public void getTriangleById_isOk() {
        Triangle expectedTriangle = triangleFactory.createRandomTriangle();
        String expectedId = expectedTriangle.getId();

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(expectedId))
                .exchange()
                .expectStatus().isOk()
                .expectBody(Triangle.class)
                .consumeWith(response -> {
                    Triangle result = response.getResponseBody();
                    assertThat("ids do not match", result.getId(), equalTo(expectedId));
                });

        triangleFactory.deleteTriangle(expectedTriangle);
    }

    @Test
    public void createTriangleUsingPOST_isOk() {

        double expectedFirstSide = 5;
        double expectedSecondSide = 6.5;
        double expectedThirdSide = 4;

        CreateTriangle createTriangle = new CreateTriangle(expectedFirstSide,expectedSecondSide, expectedThirdSide);
        Triangle expectedTriangle = webTestClient
                .post()
                .uri(qaQuizProperties.getTriangleApi())
                .bodyValue(createTriangle)
                .exchange()
                .expectBody(Triangle.class)
                .returnResult().getResponseBody();

        String expectedId = expectedTriangle.getId();

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(expectedId))
                .exchange()
                .expectStatus().isOk()
                .expectBody(Triangle.class)
                .consumeWith(response -> {
                   Triangle result = response.getResponseBody();
                   assertThat("ids do not match", result.getId(), equalTo(expectedId));
                   assertThat("firstSides do not match", result.getFirstSide(), equalTo(expectedFirstSide));
                   assertThat("secondSides do not match", result.getSecondSide(), equalTo(expectedSecondSide));
                   assertThat("firstSides do not match", result.getThirdSide(), equalTo(expectedThirdSide));
                        });

        triangleFactory.deleteTriangle(expectedTriangle);
    }

    @Test
    public void deleteTriangleById_isOk() {
        Triangle expectedTriangle = triangleFactory.createRandomTriangle();
        String expectedId = expectedTriangle.getId();

        webTestClient
                .delete()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(expectedId))
                .exchange()
                .expectStatus().isOk();

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(expectedId))
                .exchange()
                .expectStatus().isNotFound();
        
    }
    
    @Test
    /** bug QAQUIZ-1: Possible to create 11 triangles instead of 10 */
    public void createTriangleUsingPOST_checkMaximumAmountOfTriangles_is422() {

        triangleFactory.deleteAllTriangles();

        for(int i=0; i<trianglesMaxAmount; i++) {
            triangleFactory.createRandomTriangle();
        }

        CreateTriangle createTriangle = new CreateTriangle();

        int status  = webTestClient
                    .post()
                    .uri(qaQuizProperties.getTriangleApi())
                    .bodyValue(createTriangle)
                    .exchange()
                    .returnResult(Object.class)
                    .getRawStatusCode();
        triangleFactory.deleteAllTriangles();
        assertThat(status, equalTo(422));

    }
    
    @Test
    public void createTriangleUsingPOST_checkTriangleSides_negative() {

        CreateTriangle inconsistentTriangle = new CreateTriangle(5,5,20);

        webTestClient
                .post()
                .uri(qaQuizProperties.getTriangleApi())
                .bodyValue(inconsistentTriangle)
                .exchange()
                .expectStatus().is4xxClientError();
    }

    @Test
    public void createTriangleUsingPOST_checkSeparatorField_isOk() {

        double expectedFirstSide = 5;
        double expectedSecondSide = 6.5;
        double expectedThirdSide = 4;

        Random random = new Random();
        char separator = (char)(41 + random.nextInt(16));

        CreateTriangle triangle = new CreateTriangle(expectedFirstSide,expectedSecondSide, expectedThirdSide, separator);
        Triangle result = webTestClient
                .post()
                .uri(qaQuizProperties.getTriangleApi())
                .bodyValue(triangle)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Triangle.class)
                .returnResult().getResponseBody();

        assertThat("firstSides do not match", result.getFirstSide(), equalTo(expectedFirstSide));
        assertThat("secondSides do not match", result.getSecondSide(), equalTo(expectedSecondSide));
        assertThat("firstSides do not match", result.getThirdSide(), equalTo(expectedThirdSide));

        triangleFactory.deleteTriangle(result);

    }

}





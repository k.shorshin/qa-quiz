package com.natera.qa_quiz;

import com.natera.qa_quiz.configuration.properties.QaQuizProperties;
import com.natera.qa_quiz.domain.CalculatedResult;
import com.natera.qa_quiz.domain.Triangle;
import com.natera.qa_quiz.util.TriangleFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TriangleAreaPerimeterTests {

    @Autowired
    private QaQuizProperties qaQuizProperties;

    @Autowired
    private TriangleFactory triangleFactory;

    private WebTestClient webTestClient;

    @BeforeAll
    public void cleanUp() {
        triangleFactory.deleteAllTriangles();
    }

    @BeforeEach
    public void setup() {
        webTestClient = WebTestClient
                .bindToServer()
                .baseUrl(qaQuizProperties.getUri())
                .defaultHeader(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                .build();
    }

    @Test
    public void getTriangleById_CheckPerimeterApi_isOk() {

        double firstSide = 5;
        double secondSide = 4;
        double thirdSide = 3;
        double expectedPerimeter = 12;

        Triangle triangle = triangleFactory.createTriangle(firstSide, secondSide, thirdSide);

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTrianglePerimeterApi()).build(triangle.getId()))
                .exchange()
                .expectBody(CalculatedResult.class)
                .consumeWith(response -> {
                    CalculatedResult result = response.getResponseBody();
                    double perimeter = result.getResult();
                    assertThat(perimeter, equalTo(expectedPerimeter));
                });

    }

    @Test
    public void getTriangleById_CheckAreaApi_isOk() {

        double firstSide = 5;
        double secondSide = 4;
        double thirdSide = 3;
        double expectedArea = 6;

        Triangle triangle = triangleFactory.createTriangle(firstSide, secondSide, thirdSide);

        webTestClient
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleAreaApi()).build(triangle.getId()))
                .exchange()
                .expectBody(CalculatedResult.class)
                .consumeWith(response -> {
                    CalculatedResult result = response.getResponseBody();
                    double perimeter = result.getResult();
                    assertThat(perimeter, equalTo(expectedArea));
                });

    }
}

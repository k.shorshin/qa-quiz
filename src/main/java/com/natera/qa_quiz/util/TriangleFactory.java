package com.natera.qa_quiz.util;

import com.natera.qa_quiz.configuration.properties.QaQuizProperties;
import com.natera.qa_quiz.domain.CreateTriangle;
import com.natera.qa_quiz.domain.Triangle;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
@Component
public class TriangleFactory {

    @Autowired
    private QaQuizProperties qaQuizProperties;

    private WebClient webClient;

    private WebClient getWebClient() {
        if (webClient==null) {
        webClient = WebClient.create(qaQuizProperties.getUri()); }
                return webClient;

    }

    public Triangle createRandomTriangle() {

            CreateTriangle createTriangle = new CreateTriangle();

            Triangle result = getWebClient()
                    .post()
                    .uri(qaQuizProperties.getTriangleApi())
                    .header(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                    .bodyValue(createTriangle)
                    .exchange()
                    .block()
                    .bodyToMono(Triangle.class)
                    .block();

            return result;

    }

    public Triangle createTriangle(double firstSide, double secondSide, double thirdSide) {

        CreateTriangle createTriangle = new CreateTriangle(firstSide, secondSide, thirdSide);

        Triangle result = getWebClient()
                .post()
                .uri(qaQuizProperties.getTriangleApi())
                .header(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                .bodyValue(createTriangle)
                .exchange()
                .block()
                .bodyToMono(Triangle.class)
                .block();

        return result;

    }


    public Triangle[] getAllTriangles() {
        Triangle[] getAll = getWebClient()
                .get()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build("all"))
                .header(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                .exchange()
                .block()
                .bodyToMono(Triangle[].class)
                .block();
        return getAll;
    }

    public void deleteTriangle(Triangle triangle) {
        getWebClient()
                .delete()
                .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(triangle.getId()))
                .header(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                .exchange()
                .block();

    }

    public void deleteAllTriangles() {
        Triangle[] getAll = getAllTriangles();

        for (int i = 0; i < getAll.length; i++) {
            String id = getAll[i].getId();
            getWebClient()
                    .delete()
                    .uri(UriBuilder ->  UriBuilder.path(qaQuizProperties.getTriangleIdApi()).build(id))
                    .header(qaQuizProperties.getUser(), qaQuizProperties.getToken())
                    .exchange()
                    .block();
        }
    }

}



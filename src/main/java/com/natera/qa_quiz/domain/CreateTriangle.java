package com.natera.qa_quiz.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Getter
@Setter
public class CreateTriangle {

    @JsonProperty("separator")
    private char separator;

    @JsonProperty("input")
    private String input;

    public CreateTriangle() {

        Random random = new Random();
        double firstSide = random.nextInt(10)+2.5;
        double secondSide = firstSide - 1;
        double thirdSide = firstSide + 1;
        this.separator = ';';
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstSide).append(this.separator).
                append(secondSide).append(this.separator).append(thirdSide);
        this.input = stringBuilder.toString();

    }

    public CreateTriangle(double firstSide, double secondSide, double thirdSide) {

        this.separator = ';';
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstSide).append(this.separator).
                append(secondSide).append(this.separator).append(thirdSide);
        this.input = stringBuilder.toString();

    }

    public CreateTriangle(double firstSide, double secondSide, double thirdSide, char separator) {

        this.separator = separator;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(firstSide).append(this.separator).
                append(secondSide).append(this.separator).append(thirdSide);
        this.input = stringBuilder.toString();

    }


    @Override
    public String toString() {
       String result =  "{\"separator\": \""+this.separator+", \"input\": " + this.input + "\"}";
       return result;
    }
}

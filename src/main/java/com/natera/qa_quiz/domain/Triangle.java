package com.natera.qa_quiz.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Triangle {

    @JsonProperty("id")
    private String id;

    @JsonProperty("firstSide")
    private double firstSide;

    @JsonProperty("secondSide")
    private double secondSide;

    @JsonProperty("thirdSide")
    private double thirdSide;


}

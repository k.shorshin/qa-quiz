package com.natera.qa_quiz.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class CalculatedResult {

    @JsonProperty("result")
    private double result;
}

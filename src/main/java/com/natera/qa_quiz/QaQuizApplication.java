package com.natera.qa_quiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QaQuizApplication {

    public static void main(String[] args) {
        SpringApplication.run(QaQuizApplication.class, args);
    }
}


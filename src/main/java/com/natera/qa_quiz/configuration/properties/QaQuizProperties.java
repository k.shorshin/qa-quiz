package com.natera.qa_quiz.configuration.properties;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "qa-quiz")
@Component
@Getter
@Setter
public class QaQuizProperties {

    @NotBlank
    private String uri;

    @NotBlank
    private String triangleApi;

    @NotBlank
    private String triangleIdApi;

    @NotBlank
    private String triangleAreaApi;

    @NotBlank
    private String trianglePerimeterApi;

    @NotBlank
    private String user;

    @NotBlank
    private String token;

}

# Test automation framework for Natera Qa-quiz

Java + SpringBootTest + JUnit5

---
# Tickets report:
### QAQUIZ-1
*Summary:* Possible to create 11 triangles instead of 10<br/>
*Priority:* Medium<br/>
*Test environment:* https://qa-quiz.natera.com  <br/>
*Bug type:* Software
#### Steps to reproduce:
1. Clear environment
2. Create 10 triangles:
send POST request with body {"input":"5;4;3"} to /triangle endpoint 10 times
3. Send same POST request to create 11th triangle
#### Actual result:
Triangle created successfully

#### Expected result:
422 Unprocessible entry

    {
        "timestamp": 1606151992063,
        "status": 422,
        "error": "Unprocessable Entity",
        "exception": "com.natera.test.triangle.exception.LimitExceededException",
        "message": "Limit exceeded",
        "path": "/triangle"
    }
#### Source of expectation:
https://qa-quiz.natera.com/
>You can save up to 10 triangles

### QAQUIZ-2
*Summary:* DELETE to /triangle/{triangleId} always returns 200 Ok<br/>
*Priority:* Low<br/>
*Test environment:* https://qa-quiz.natera.com  <br/>
*Bug type:* Software
#### Steps to reproduce:
Send DELETE to /triangle/{triangleId} endpoint with random id

#### Actual result:
Response 200 Ok

#### Expected result:
404 not found

#### Source of expectation:
https://qa-quiz.natera.com/

### QAQUIZ-3
*Summary:* Range of possible separator symbols is not defined <br/>
*Priority:* Low<br/>
*Test environment:* https://qa-quiz.natera.com  <br/>
*Bug type:* Documentation
#### Steps to reproduce:
Send POST request with body {"separator": "+", "input":"5+4+3"} to /triangle endpoint

#### Actual result:
500 Internal Server Error

    {
        "timestamp": 1606152679325,
        "status": 500,
        "error": "Internal Server Error",
        "exception": "java.util.regex.PatternSyntaxException",
        "message": "Dangling meta character '+' near index 0\n+\n^",
        "path": "/triangle"
    }
Same for "*" as a separator

#### Expected result:
200 Ok
It is needed to specify a list of possible separators

#### Source of expectation:
https://qa-quiz.natera.com/

### QAQUIZ-4
*Summary:* 500 error with NullPointer Exception for typo in "input" field while creating a new triangle<br/>
*Priority:* Low<br/>
*Test environment:* https://qa-quiz.natera.com  <br/>
*Bug type:* Software
#### Steps to reproduce:
Send POST request with typo error in "input" field in body {"inputt":"5+4+3"} to /triangle endpoint

#### Actual result:
500 Internal Server Error

    {
        "timestamp": 1606152938308,
        "status": 500,
        "error": "Internal Server Error",
        "exception": "java.lang.NullPointerException",
        "message": "No message available",
        "path": "/triangle"
    }

#### Expected result:
422 Unprocessible entry

#### Source of expectation:
https://qa-quiz.natera.com/
>422	Unprocessible	Typo in a request?